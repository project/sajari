# INTRODUCTION
---------------------------
Search.io module provides a wrapper for [Search.io search engine](http://search.io) to be used on Drupal sites.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/sajari

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/sajari

# REQUIREMENTS
-----------------------------------------------
This module requires the following Drupal module:

* JSON Template (https://drupal.org/project/json_template)

This module requires also the following Javascript libraries to be installed in /libraries folder:

* react (https://github.com/facebook/react)
* react-dom (https://github.com/facebook/react)
* handlebars (https://github.com/handlebars-lang/handlebars.js)

It is suggested to install those libraries using Composer from [Asset Packagist](https://asset-packagist.org/) More on using Asset Packagist with Drupal [here](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries)

# RECOMMENDED MODULES
-------------------

 * Markdown filter (https://www.drupal.org/project/markdown): When enabled, display of the project's README.md help will be rendered with markdown.

# INSTALLATION
----------------

Install the libraries listed in Requirements section, and, if not using Composer, required Drupal module(s), then install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.

# CONFIGURATION
---------------

First, you need to set up things at Search.io backend. [Get started](https://www.search.io/docs/user-guide/getting-started/get-started/) guide there should introduce you to the first steps. You need just to set up a collection, all the rest our module will take care for you.

Then, you need to configure project and collection that will be used for all searches on your site. Technically, that allows to search not only the current site, but some totally different, or some more general domain or collection of sites. This configuration is at /admin/config/search/sajari.

Displaying search box on the site is implemented with a block called "Search.io search" within "Search" category (block plugin id is 'sajari_search'). This block is highly configurable and can be placed according to normal block placement rules. For the options block provides please consult Search.io documentation at [search.io](http://search.io).

Search result theming is made through templates you can select in the block configuration. They are handled with [JSON Template](https://drupal.org/project/json_template) module. See sajari.json_template.templates.yml and templates/sajari_results.html.hbs for an example. You can declare templates the same way in your module or theme (changing "sajari" to module or theme name and "sajari_results" to the name of your template). If you want to use other templating engine than handlebars, you'll need to implement a transformer for JSON Template module (see documentation there).

If you want to add instant indexing feature (https://www.search.io/solutions/features/crawling) to some or all pages of your site, place block "Search.io indexing" (block plugin id 'sajari_indexer') on those pages.

# MAINTAINERS
-----------

Current maintainers:
 * Vadim Valuev (gease) - https://www.drupal.org/u/gease

This project has been sponsored by:
 * MORPHT
   Morpht is a Sydney based Drupal web development consultancy providing quality Drupal websites and Drupal consulting services to a variety of clients. Our strengths are around project scoping, information architecture, site building, custom module development, data migration, design, theming and dev ops. Visit [morpht.com](http://morpht.com) for more information.
